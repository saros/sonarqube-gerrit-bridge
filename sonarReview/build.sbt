organization := "org.agse"

name := "sonarReview"

version := "0.0.1"

scalaVersion := "2.10.3"

libraryDependencies += "org.rogach" %% "scallop" % "0.6.3"

seq(com.github.retronym.SbtOneJar.oneJarSettings: _*)

mainClass in oneJar := Some("org.agse.sonarreview.sonarReview")

libraryDependencies += "commons-lang" % "commons-lang" % "2.6"

libraryDependencies += "com.stackmob" %% "newman" % "1.3.5"

libraryDependencies += "org.parboiled" % "parboiled-scala_2.10" % "1.1.5"

libraryDependencies += "org.ccil.cowan.tagsoup" % "tagsoup" % "1.2" 

libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging-slf4j" % "2.1.2"

libraryDependencies += "org.eclipse.jgit" % "org.eclipse.jgit" % "3.3.2.201404171909-r"

libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.1.2"

libraryDependencies += "org.agse" % "gitclient_2.10" % "0.0.1" 

libraryDependencies += "com.typesafe" % "config" % "1.2.1"

libraryDependencies += "org.specs2" % "specs2_2.10" % "2.4.16" % "test"

resolvers ++= Seq(
  "Scalaz Bintray Repo"  at "http://dl.bintray.com/scalaz/releases",
  "Typesafe" at "http://repo.typesafe.com/typesafe/releases/"
)
