package org.agse.sonarreview
import org.agse.gitclient.{ GitRepo }
import com.typesafe.scalalogging.slf4j._
import net.liftweb.json._
import JsonDSL._
import scala.xml._
import com.typesafe.config.Config

/**
 * Parses Sonar Issues Reports.
 * For reference see this issuesreport.ftl as found in
 * https://github.com/SonarCommunity/sonar-issues-report/blob/1b38791/src/main/resources/org/sonar/issuesreport/printer/html/issuesreport.ftl
 */

class IssuesParser(repo: GitRepo, config: Config) extends LazyLogging {
  // this particular parser can parse "tag-soup" document, which old html is
  val xml = XML.withSAXParser(new org.ccil.cowan.tagsoup.jaxp.SAXFactoryImpl().newSAXParser())

  def projects = repo.lastCommit.affectedProjects
  logger.debug(s"Projects affected by last commit:\n${projects.show}")

  def changedFiles = repo.lastCommit.affectedFiles
  logger.debug(s"Files affected by last commit:\n${changedFiles.show}")

  def isMerge = repo.lastCommit.isMerge
  logger.debug(s"Last commit is a merge commit? $isMerge")

  def parse(changeId: String): String =
    pretty(
      render(
        merged(
          projects map (parseProject(changeId, _)))))

  def parse(changeId: String, project: String): String =
    if (!isMerge)
      pretty(
        render(
          parseProject(changeId, project)))
    else
      pretty(
        render(
          mergeCommitReply))

  val mergeCommitReply = ("message" -> "Nothing to analyze, since this is a merge commit.")

  private def parseProject(changeId: String, project: String): JValue = {

    val pathToReport = config.getString(s"projects.$project.issueReports") +
      changeId + "/" +
      project +
      "/issues-report-light.html"
    val issuesReport = xml.load(pathToReport)
    // this element is always there
    val summaryPerFile = (issuesReport \\ "div") find { x => (x \ "@id").text == "summary-per-file" }
    // summary-per-file has only a series of tables as children
    // each such table corresponds to a resource
    val resourceReports = summaryPerFile.flatMap(x => Some(x \ "table"))

    val comments = merged(resourceReports.get map commentsPerResource)

    val projectTitle = config.getString(s"projects.$project.projectTitle")

    if (comments == JNothing)
      ("message" -> s"Congratulations, QA found no issues whatsoever in $projectTitle!")
    else
      ("message" -> s"Please review your QA analysis results for $projectTitle") ~
        ("comments" -> comments)
  }

  private def commentsPerResource(resourceReport: Node): JValue = {
    val sources = (resourceReport \\ "table") find
      { x => (x \ "@class").text == "sources" }
    val linesWithIssues = (sources.get \\ "tr") filter
      { x => (x \ "@id").text.matches("[0-9]*LV[0-9]*") }

    val name = (resourceReport \ "thead" \ "tr" \ "th" \ "div" \ "a").text

    logger.debug(s"checking whether $name is contained in commit")

    if (changedFiles contains name)
      (name -> merged(linesWithIssues map commentsPerLine))
    else
      JNothing
  }

  private def commentsPerLine(lineWithIssues: Node): JValue = {

    val issues = (lineWithIssues \ "td" \ "div") filter
      { x => (x \ "@class").text == "issue" }

    val id = (lineWithIssues \ "@id").text
    val lineNumber = (id: String) => id.substring(id.lastIndexOf("V") + 1)

    issues map (extractComments(lineNumber(id), _: Node))
  }

  private def extractComments(line: String, issue: Node): JObject = {
    // the span with class "rulename" contains the rulename
    val ruleName = ((issue \\ "span") find
      { x => (x \ "@class").text == "rulename" }).get.text
    val discussionComment = ((issue \\ "div") find
      { x => (x \ "@class").text == "discussionComment" }).get.text
    val severityIcon = ((issue \\ "i" \ "@class") find
      { x => x.text.matches("icon-severity-(blocker|critical|major|minor|info)") }).get.text
    logger.debug(s"found severity icon $severityIcon")

    val pattern = """(?<=icon-severity-).*""".r
    val severity = pattern.findAllIn(severityIcon).next

    ("line" -> line) ~
      ("message" -> (severity.capitalize + "\n\n" + discussionComment.trim + "\n\n " + ruleName.trim))
  }

  protected def merged(jValues: Iterable[JValue]) = jValues.foldLeft(JNothing: JValue)(_ merge _)
}
