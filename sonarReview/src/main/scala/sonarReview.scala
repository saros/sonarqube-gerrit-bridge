package org.agse.sonarreview
import org.rogach.scallop._
import scala.xml._
import org.agse.gitclient.{ GitRepo }
import com.typesafe.config._
import java.io.File

object sonarReview {

  def main(args: Array[String]) {
    object P extends ScallopConf(args) {
      version("Version 0.0.1")
      banner("""Usage: sonarreview.jar --change|c GERRIT_CHANGE_NUMBER --revision|-r GERRIT_PATCHSET_NUMBER --project|-p PROJECT [--debug|-d]
           | 
           |sonarReview uses a configuration file which must lie in the same directory from where it is being called
           |and it must be named "application.conf". It must contain the following (${port} being optional)
           |
           |gerrit {
           |    host = the server on which gerrit is hosted
           |    user = the username, as which you want to post the review
           |    pass = the password for the username above
           |    port = the port on which to post to the gerrit server, omitting this defaults to port 80
           |}
           | 
           |gitRepoLocation = absolute path to the location of the .git folder of your current project
           | 
           |projects{
           |    projectFolder {
           |        projectTitle = the human-readable version of the project title
           |        issueReports = absolute path to the location of the folder where issue reports should be found
           |    }
           |}
           |It will look for issues reports under ${issueReports}/GERRIT_CHANGE_NUMBER, parse them and post 
           |the issues found therein as comments to ${host}:${port}/gerrit as the user identified by ${user} and 
           |${pass} for the patchset revision GERRIT_PATCHSET_NUMBER.
           |
           |Parameters:
           |""".stripMargin)

      val changeNumber = opt[String]("change", required = true,
        descr = "the GERRIT_CHANGE_NUMBER environment variable, as provided by a Gerrit-triggered Jenkins job")
      val patchsetNumber = opt[String]("revision", required = true,
        descr = "the GERRIT_PATCHSET_NUMBER environment variable, as provided by a Gerrit-triggered Jenkins job")
      val project = opt[String]("project", required = false,
        descr = """the folder of the given subproject, if not specified comments are gathered for all projects 
          |affected by the last commit""")
      val debug = toggle("debug", default = Some(false))
      verify
    }

    import org.slf4j.{ LoggerFactory }
    import ch.qos.logback.classic.{ Level, Logger }

    val root = (LoggerFactory getLogger ("ROOT")).asInstanceOf[ch.qos.logback.classic.Logger]
    if (P.debug.apply)
      root.setLevel(Level.DEBUG)
    else
      root.setLevel(Level.INFO)

    val config = ConfigFactory.parseFile(new File("application.conf"))

    val git = new GitRepo(config.getString("gitRepoLocation"))

    val issuesParser = new IssuesParser(git, config)

    val comments = P.project.get match {
      case Some(project) =>
        issuesParser.parse(P.changeNumber.apply, project)
      case None =>
        issuesParser.parse(P.changeNumber.apply)
    }

    val gerrit = new GerritRestClient(config)
    gerrit.setReview(P.changeNumber.apply, P.patchsetNumber.apply, comments)

    System.exit(0)
  }
}
