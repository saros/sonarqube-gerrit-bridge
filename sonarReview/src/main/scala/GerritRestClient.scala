package org.agse.sonarreview
import com.stackmob.newman._
import response.{ HttpResponse, HttpResponseCode }
import request.{ HttpRequest }
import dsl._
import com.typesafe.scalalogging._
import com.typesafe.scalalogging.slf4j._
import org.parboiled.common.Base64
import scala.concurrent._
import ExecutionContext.Implicits.global
import duration._

import scala.xml._
import scala.util.{ Success, Failure }

import com.typesafe.config.Config

class GerritRestClient(conf: Config) extends LazyLogging {

  //required
  private val defaultHost = conf.getString("gerrit.host")
  private val defaultUser = conf.getString("gerrit.user")
  private val defaultPass = conf.getString("gerrit.pass")
  //optional
  private val defaultPort = if (conf.hasPath("gerrit.port")) conf.getString("gerrit.port") else ""

  def setReview(changeId: String, patchsetRevision: String,
    comments: String, host: String = defaultHost,
    user: String = defaultUser, pass: String = defaultPass, port: String = defaultPort) = {

    val path = "gerrit/a/changes/" + changeId + "/revisions/" + patchsetRevision + "/review"

    val bytes = (defaultUser + ":" + defaultPass).getBytes
    val encoded = Base64.rfc2045.encodeToString(bytes, false)
    val headers = List(
      "Authorization" -> ("Basic " + encoded),
      "Content-Type" -> "application/json;charset=UTF-8")

    val request =
      if (!defaultPort.isEmpty)
        POST(url(http, host, defaultPort.toInt, path)).
          addHeaders(headers).
          addBody(comments)
      else
        POST(url(http, host, path)).
          addHeaders(headers).
          addBody(comments)

    logger.debug(s"Posting request: ${request.show}")

    val response = request.apply

    response onFailure {
      case _ => System.exit(1)
    }

    response onComplete {
      case Success(r) => r match {
        case HttpResponse(HttpResponseCode.Ok, _, _, _) =>
          logger.info("Posting comments to Gerrit: Success!")
        case _ =>
          logger.error(
            s"Response from Gerrit:\n ${r.code} \n" +
              s"${r.headers.get} \n" +
              s"${r.rawBody} \n " +
              s"for request ${request.show}")
      }
      case Failure(f) => logger.trace("POSTing to Gerrit failed")
    }
    Await.result(response, Duration.Inf)
  }
}
