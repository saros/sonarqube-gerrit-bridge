package org.agse.sonarreview
import com.stackmob.newman._
import request.{ HttpRequest }
import dsl._
import net.liftweb.json._

object `package` {
  implicit val httpClient = new ApacheHttpClient

  implicit class IterableAug(val i: Iterable[Any]) {
   def show = i.foldRight("")(_ + "\n" + _.toString)
  }

  implicit class RequestAug(val req: HeaderAndBodyBuilder) {
    def show = {

      lazy val json = req.toJson(true)
      lazy val parsed = parse(json)
      lazy val requestToString = (r: HttpRequest, keys: Seq[String]) =>
        keys.foldLeft("")((req, key) => req + "\n" + pretty(render(parsed \ key)))

      requestToString(req, List("type", "url", "body"))
    }
  }
}

    

