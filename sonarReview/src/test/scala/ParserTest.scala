package org.agse.sonarreview

import org.agse.gitclient.GitRepo
import net.liftweb.json._
import org.specs2.mutable._
import org.specs2.mock.Mockito
import com.typesafe.config._

object ParserTest extends Specification with Mockito {
  sequential

  val affectedFile = "de.fu_berlin.inf.dpp.core/src/de/fu_berlin/inf/dpp/ClassThatBreaksACoupleOfSonarRules.java"
  val affectedProject = "de.fu_berlin.inf.dpp.core"

  val mockCommit = mock[mockRepo.Commit]

  mockCommit.affectedProjects returns
    Set(affectedProject)

  mockCommit.affectedFiles returns
    Set() thenReturns
    Set() thenReturns
    Set(affectedFile)

  mockCommit.isMerge returns
    true thenReturns
    true thenReturns false

  val mockRepo = mock[GitRepo]
  mockRepo.lastCommit returns (mockCommit)

  val pathToReports = "src/test/resources/"
  val changeId = "sampleReport"
  val project = "de.fu_berlin.inf.dpp.core"

  val mockConfig = mock[Config]
  mockConfig.getString(s"projects.$project.issueReports") returns pathToReports
  mockConfig.getString(s"projects.$project.projectTitle") returns "Saros Core"

  val issuesParser = new IssuesParser(mockRepo, mockConfig)

  "IssuesParser" should {
    "not analyze merge commits" in {
      val review = issuesParser.parse(changeId, project)
      compact(render(parse(review) \ "message")) mustEqual "\"Nothing to analyze, since this is a merge commit.\""
    }

    "produce a confirmatory review message when no issues are found" in {
      val review = issuesParser.parse(changeId, project)
      compact(render(parse(review) \ "message")) mustEqual "\"Congratulations, QA found no issues whatsoever in Saros Core!\""
    }

    "produce a review message that prompts to review the issues found" in {
      val review = issuesParser.parse(changeId, project)
      compact(render(parse(review) \ "message")) mustEqual "\"Please review your QA analysis results for Saros Core\""
    }

    "produce a comment for each line with an issue" in {
      val numberOfIssues = 12
      val review = issuesParser.parse(changeId, project)
      (parse(review) \\ "line").children.length mustEqual numberOfIssues
      ((parse(review) \\ "message").children.length - 1) mustEqual numberOfIssues
    }

    "produce comments when issues are found" in {
      val lengthOfReview = 1957
      val review = issuesParser.parse(changeId, project)
      compact(render(parse(review))).length mustEqual lengthOfReview
    }
  }
}
