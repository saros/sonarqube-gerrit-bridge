package org.agse.gitclient
import org.rogach.scallop._

object gitClient {

  def main(args: Array[String]) {
    object P extends ScallopConf(args) {
      version("Version 0.0.1")
      banner("""Usage: gitclient.jar --contains|c PROJECT
           | 
           | gitclient interfaces with the git repo it is being called in.
           | Currently, it only tells you whether a project is contained in
           | the last commit to this repo.
           | Returns 0 when the project has been modified in the last commit
           | and 1 when not
           |Parameters:
           |""".stripMargin)

      val project = opt[String]("contains", required = false,
        descr = "the project folder, which is a direct subfolder saros/")
      val merge = toggle("merge", default = Some(false))
      val debug = toggle("debug", default = Some(false))
      mutuallyExclusive(project, merge)
      verify
    }

    import org.slf4j.{ LoggerFactory }
    import ch.qos.logback.classic.{ Level, Logger }

    val root = (LoggerFactory getLogger ("ROOT")).asInstanceOf[ch.qos.logback.classic.Logger]
    if (P.debug.apply)
      root.setLevel(Level.DEBUG)
    else
      root.setLevel(Level.INFO)

    val repo = new GitRepo
    if (P.merge.apply)
      if (repo.lastCommit.isMerge)
        System.exit(0)
      else
        System.exit(1)
    else if (repo.lastCommit.affectedProjects contains P.project.apply)
      System.exit(0)
    else
      System.exit(1)

  }
}
