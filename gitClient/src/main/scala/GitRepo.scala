package org.agse.gitclient
import java.io.File
import org.eclipse.jgit._
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.diff.DiffEntry.{ ChangeType, Side }
import org.eclipse.jgit.lib.{ ObjectId, ObjectReader, Repository }
import org.eclipse.jgit.revwalk.{ RevCommit, RevTree, RevWalk }
import org.eclipse.jgit.storage.file.FileRepositoryBuilder
import org.eclipse.jgit.treewalk.{ AbstractTreeIterator, CanonicalTreeParser }
import scala.collection._
import JavaConversions._
import com.typesafe.scalalogging.slf4j._

class GitRepo(repoLocation: String = ".git/") {
  private val repo = (new FileRepositoryBuilder).setGitDir(new File(repoLocation)).build
 
  private lazy val _lastCommit = new Commit("HEAD")

  def lastCommit:Commit = _lastCommit
 
  class Commit(newCommit: String, oldCommit: String) {
    
    def this(newCommit: String) = this(newCommit, newCommit + "~")

    private val contributionTypes = Set(ChangeType.ADD, ChangeType.MODIFY)
    private val prevHead = prepareTreeParser(repo, resolve(oldCommit))
    private val head = prepareTreeParser(repo, resolve(newCommit))
    private val nameAndStatusOnly = new Git(repo).
      diff.
      setOldTree(prevHead).
      setNewTree(head).
      setShowNameAndStatusOnly(true).
      call

    def affectedFiles = (nameAndStatusOnly.filter(contributionTypes contains _.getChangeType) map
      (_.getPath(Side.NEW))).
      toSet

    def commit = getCommit(repo, resolve(newCommit))
    def isMerge = commit.getParentCount > 1
      
    private lazy val getProjectFolder = (x: String) => if(x.contains("/")) x.substring(0, x.indexOf("/")) else ""
    def affectedProjects = affectedFiles map getProjectFolder
  }

  private def resolve = (ref: String) => repo.resolve(ref).name

  private def getCommit(repository: Repository, objectId: String): RevCommit = 
    getCommitAndParseTree(repository, objectId)._1
  
  private def getParseTree(repository: Repository, objectId: String): RevTree = 
    getCommitAndParseTree(repository, objectId)._2

  private def getCommitAndParseTree(repository: Repository, objectId: String): (RevCommit, RevTree) = {
    val walk: RevWalk = new RevWalk(repository)
    val commit: RevCommit = walk.parseCommit(ObjectId.fromString(objectId))
    val tree: RevTree = walk.parseTree(commit.getTree.getId)
    (commit, tree)
  }

  // borrowed from https://github.com/centic9/jgit-cookbooks, Apache Licence
  private def prepareTreeParser(repository: Repository, objectId: String): AbstractTreeIterator = {
    val tree = getParseTree(repository, objectId)

    val oldTreeParser: CanonicalTreeParser = new CanonicalTreeParser
    val oldReader: ObjectReader = repository.newObjectReader
    try {
      oldTreeParser.reset(oldReader, tree.getId)
    } finally {
      oldReader.release
    }
      oldTreeParser
  }
}
