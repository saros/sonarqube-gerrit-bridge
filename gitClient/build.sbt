organization := "org.agse"

name := "gitClient"

version := "0.0.1"

scalaVersion := "2.10.3"

libraryDependencies += "org.rogach" %% "scallop" % "0.6.3"

seq(com.github.retronym.SbtOneJar.oneJarSettings: _*)

mainClass in oneJar := Some("org.agse.gitclient.gitClient")

libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging-slf4j" % "2.1.2"

libraryDependencies += "org.eclipse.jgit" % "org.eclipse.jgit" % "3.3.2.201404171909-r"

libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.1.2"
 
